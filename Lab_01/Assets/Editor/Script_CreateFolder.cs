﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class Script_CreateFolder : MonoBehaviour
{

    [MenuItem("Tool Creation/Create folder")]


        public static void Createfolder()
    {
                   
        //Create a folder named Dynamic Assets in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Dynamic Assets");
        //Create a file named "folderStructure.txt" in the Assets/Dynamic Assets folder
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/folderStructure.txt", "This folder is for storing Dynamic Assets Folders!");

        //Create a folder named Resources in the Dynamic Assets folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets", "Resources");
        //Create a file named "folderStructure.txt" in the Assets/Dynamic Assets/Resources folder
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/folderStructure.txt", "This folder is for storing Resource Assets!");

        //Create a folder named Animations in the Assets/Dynamic Assets/Resources folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animations");

        //Create a folder named Sources in the Assets/Dynamic Assets/Resources/Animations folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Animations", "Sources");

        //Create a folder named Animation Controllers in the Assets/Dynamic Assets/Resources folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animation Controllers");

        //Create a folder named Effects in the Assets/Dynamic Assets/Resources folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Effects");

        //Create a folder named Models in the Assets/Dynamic Assets/Resources folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Models");

        //Create a folder named Character in the Assets/Dynamic Assets/Resources/Models folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Character");

        //Create a folder named Environment in the Assets/Dynamic Assets/Resources/Models folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Environment");
             
        //Create a folder named Prefabs in the Assets/Dynamic Assets/Resources folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Prefabs");

        //Create a folder named Common in the Assets/Dynamic Assets/Resources/Prefabs folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Prefabs", "Common");
                
        //Create a folder named Sounds in the Assets/Dynamic Assets/Resources folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Sounds");

        //Create a folder named Music in the Assets/Dynamic Assets/Resources/Sounds folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "Music");

        //Create a folder named Common in the Assets/Dynamic Assets/Resources/Sounds/Music folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/Music", "Common");

        //Create a folder named SFX in the Assets/Dynamic Assets/Resources/Sounds folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "SFX");

        //Create a folder named Common in the Assets/Dynamic Assets/Resources/Sounds/SFX folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/SFX", "Common");

        //Create a folder named Textures in the Assets/Dynamic Assets/Resources folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Textures");

        //Create a folder named Common in the Assets/Dynamic Assets/Resources/Textures folder
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Textures", "Common");
        
        //Create a file named "folderStructure.txt" in the Assets/Editor folder
        System.IO.File.WriteAllText(Application.dataPath + "/Editor/folderStructure.txt", "This folder is for storing Editor materials!");

        //Create a folder named Exstensions in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Extensions");
        //Create a file named "folderStructure.txt" in the Assets/Extensions folder
        System.IO.File.WriteAllText(Application.dataPath + "/Extensions/folderStructure.txt", "This folder is for storing extensions!");

        //Create a folder named Gizmos in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Gizmos");
        //Create a file named "folderStructure.txt" in the Assets/Gizmos folder
        System.IO.File.WriteAllText(Application.dataPath + "/Gizmos/folderStructure.txt", "This folder is for storing gizmos!");

        //Create a folder named Plugins in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Plugins");
        //Create a file named "folderStructure.txt" in the Assets/Plugins folder
        System.IO.File.WriteAllText(Application.dataPath + "/Plugins/folderStructure.txt", "This folder is for storing plugins!");

        //Create a folder named Scripts in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Scripts");
        //Create a file named "folderStructure.txt" in the Assets/Scripts folder
        System.IO.File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt", "This folder is for storing Scripts!");

        //Create a folder named Shaders in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Shaders");
        //Create a file named "folderStructure.txt" in the Assets/Shaders folder
        System.IO.File.WriteAllText(Application.dataPath + "/Shaders/folderStructure.txt", "This folder is for storing shaders!");

        //Create a folder named Static Assets in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Static Assets");
        //Create a file named "folderStructure.txt" in the Assets/Static Assets folder
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/folderStructure.txt", "This folder is for storing Static Assets!");

        //Create a folder named Animations in the Assets/Static Assets folder
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animations");

        //Create a folder named Sources in the Assets/Static Assets/Animations folder
        AssetDatabase.CreateFolder("Assets/Static Assets/Animations", "Sources");

        //Create a folder named Animation Controllers in the Animations/Static Assets folder
        AssetDatabase.CreateFolder("Assets/Static Animations", "Animation Controllers");

        //Create a folder named Effects in the Assets/Static Assets folder
        AssetDatabase.CreateFolder("Assets/Static Assets", "Effects");

        //Create a folder named Models in the Assets/Static Assets folder
        AssetDatabase.CreateFolder("Assets/Static Assets", "Models");

        //Create a folder named Character in the Assets/Static Assets/Models folder
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Character");

        //Create a folder named Environment in the Assets/Static Assets/Models folder
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Environment");

        //Create a folder named Prefabs in the Assets/Static Assets folder
        AssetDatabase.CreateFolder("Assets/Static Assets", "Prefabs");

        //Create a folder named Common in the Assets/Static Assets/Prefabs folder
        AssetDatabase.CreateFolder("Assets/Static Assets/Prefabs", "Common");

        //Create a folder named Scenes in the Assets/Static Assets folder
        AssetDatabase.CreateFolder("Assets/Static Assets", "Scenes");

        //Create a folder named Sounds in the Assets/Static Assets folder
        AssetDatabase.CreateFolder("Assets/Static Assets", "Sounds");

        //Create a folder named Music in the Assets/Static Assets/Sounds folder
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "Music");

        //Create a folder named Common in the Assets/Static Assets/Sounds/Music folder
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/Music", "Common");

        //Create a folder named SFX in the Assets/Static Assets/Sounds folder
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "SFX");

        //Create a folder named Common in the Assets/Static Assets/Sounds/SFX folder
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/SFX", "Common");

        //Create a folder named Textures in the Assets/Static Assets folder
        AssetDatabase.CreateFolder("Assets/Static Assets", "Textures");

        //Create a folder named Common in the Assets/Static Assets/Textures folder
        AssetDatabase.CreateFolder("Assets/Static Assets/Textures", "Common");

        //Create a folder named Testing in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Testing");
        
        //Refresh the project structure to commit all changes
        AssetDatabase.Refresh();
    }
}


/// Post-Lab Answers
/// 1. Holds all the scripts that alter the unity editor
/// 
/// 2. [MenuItem("Name of Toolbar Menu Item/Item Name")]
/// 
/// 3.Creating duplicate items
/// 
/// 4.Call a public static void function
/// 
/// 5.To update all the chages in the editor
/// 
/// 6.Dynamic assets are created during run time and static are part of the program
/// 
/// 7.Dynamic assets are not stored on the hard drive since they are created during run-time.
/// 
/// 8. Assets is broken down into Dynamic and Static; Everything else is classified into those two categories. 
/// 

